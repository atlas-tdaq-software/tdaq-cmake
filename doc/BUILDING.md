# Build Instructions

For ATLAS user the [twiki](//twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltCMake) contains
the instructions on how the release is built by librarians and the gitlab CI infrastructure.

This is a short re-cap for users not in ATLAS.

## Warning: if you are not in ATLAS, or even High Energy Physics

You probably do not want to build this whole software stack. No really, you don't. A large
part of it is only usable in the context of the ATLAS data taking environment and
lots of it talks to custom hardware that you cannot buy, or that has long ago reached
its end of life.

For a reasonable 'run control' subset look at the package list in `cmake/variants/arm/packages.txt`.
You can choose this by adding the `--variant=arm` option to the `build_stack` command below.

## Warning: Disk and Memory

A full build takes about 15 GByte for the build area, and about 2 GByte for the install area.

A few targets have excessive memory requirements, i.e. 5-6 GByte of RSS per compile job.
The build scripts run by default with the number of cores on the system. Depending on your
overall memory the OOM killer may kick. See below on how to reduce the number of parallel make
jobs.

## Standard Environment

This assumes that the builds happens on an Alma 9 machine with x86_64 or aarch64 architecture and
that the CVMFS filesystem is available. The following assumes a native build. See below
for containerized builds.

Subsets of the release have been compiled for Power and ARM64 architectures, all in
little endian mode.

### OS Level Requirements

If you have access to the CERN yum repositories you can simply install the meta RPM
that will bring in all dependencies:

```bash
sudo yum install -y HEP_OSlibs
```

Otherwise run the following command:

```bash
sudo dnf install -y epel-release
sudo dnf install -y --enablerepo=cbr \
    libaio-devel \
    man-db \
    libidn2 \
    openssh-clients \
    git \
    rpm-build \
    libcurl-devel \
    alsa-lib \
    audit-libs \
    bzip2-libs \
    cracklib \
    cyrus-sasl-lib \
    device-mapper \
    e2fsprogs-libs \
    expat \
    fontconfig \
    freetype \
    gdbm \
    giflib \
    glib2 \
    glibc \
    glibc-devel \
    gmp \
    keyutils-libs \
    krb5-libs \
    libICE \
    libSM \
    libX11 \
    libXau \
    libXcursor \
    libXdamage \
    libXdmcp \
    libXext \
    libXfixes \
    libXft \
    libXi \
    libXinerama \
    libXmu \
    libXp  \
    libXpm \
    libXrandr \
    libXrender \
    libXt \
    libXtst \
    libXxf86vm \
    libaio \
    libdrm \
    libpciaccess \
    libgcc \
    libgfortran \
    libjpeg-turbo \
    libpng \
    libselinux \
    libsepol \
    libtiff \
    libuuid \
    libxcb \
    libxml2 \
    libxml2-devel \
    mesa-dri-drivers \
    mesa-dri-filesystem \
    mesa-libGL \
    mesa-libGLU \
    ncurses \
    ncurses-libs \
    nspr \
    nss \
    nss-softokn \
    nss-util \
    openldap \
    openldap-devel \
    motif-devel \
    openmotif \
    openssl \
    openssl-devel \
    pam \
    pam-devel \
    readline \
    readline-devel \
    sqlite \
    tk \
    tcl \
    zlib \
    freetype-devel \
    libXext-devel \
    libpng-devel \
    mesa-libGL-devel \
    mesa-libGLU-devel \
    ncurses-devel \
    libX11-devel \
    libXau-devel \
    libXdamage-devel \
    libXdmcp-devel \
    libXfixes-devel \
    libXxf86vm-devel \
    libdrm-devel \
    libxcb-devel \
    xorg-x11-proto-devel \
    xorg-x11-xbitmaps \
    libXaw-devel \
    libtool-ltdl \
    tcsh \
    zsh \
    procmail \
    autoconf \
    automake \
    libtool  \
    bzip2-devel \
    bind-utils \
    libuuid-devel \
    zlib-devel \
    libxslt \
    libXpm-devel \
    libXft-devel \
    krb5-devel \
    which \
    tar \
    krb5-workstation \
    net-tools \
    iproute \
    atlas \
    gnutls-devel \
    rdma-core-devel \
    libtirpc-devel \
    libnsl2-devel \
    libnsl \
    lz4-devel \
    sudo \
    pcre2-devel \
    libzstd \
    libxkbcommon-devel \
    libxkbcommon-x11-devel \
    xcb-util-devel \
    xcb-util-image-devel \
    xcb-util-keysyms-devel \
    xcb-util-renderutil-devel \
    xcb-util-wm-devel \
    libicu \
    jq \
    dbus-libs
```

### OCI Build Image.

A multi-arch docker image is available that contains all needed RPMs. Note that you still need
CVMFS access on the host machine. However, the host can run a Ubuntu, Fedora, Arch etc.
distribution.

#### Docker

```bash
docker run -it -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:el9 bash
```

#### Apptainer

You can convert the docker image into singularity image and use that instead.

```bash
apptainer pull tdaq.sif docker://gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:el9
apptainer shell -B /cvmfs tdaq.sif
```

For you convenience the docker images are also available on CVMFS:

```bash
apptainer shell -B /cvmfs /cvmfs/unpacked/gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-el9
```

### Full Stack Build

Choose a directory with lots of space and cd there. This assumes you want to build
the nightly release (whose version number is 99.0.0). Run the build script

```bash
mkdir /scratch/work
cd /scratch/work
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/build_stack --here tdaq-common-99-00-00 tdaq-99-00-00 x86_64-el9-gcc13-opt
```

All necessary tools, compilers, Java versions will be taken from /cvmfs/sft.cern.ch as needed.

An 8 core machine with 16 GByte of RAM takes about 2 hours to build and run the tests.
A 32 core machine with 64 GByte of RAM takes about half an hour to build and run the tests.

Since all O(1000) targets are visible to the top level make command, the more cores you have the better.

#### build_stack Options

 * `--here` : both the checkout and the build area are created in the current work directory (the default)
 * `--checkout=...` : explicitly specify the checkout area.
 * `--build=...` : explicitly specify the build area.
 * `--installbase=...` : normall the installation is next to the checked out code in `installed`. This allows to change this location.
 * `--dry-run` : just print what it plans to be doing.
 * `--tar` : create tar.gz files after build from install area, ready for distribution.
 * `--rpm` : create RPMs after build from install area.

#### CMake Options

All additional arguments to `build_stack` are passed to CMake until a `--` is encountered. The remaining arguments after
that are passed the `ctest` driver. Only a few useful options are given here.

  * `-D USE_CCACHE=TRUE` : enable ccache. This can bring repeated builds down to ~15min each even on a laptop.
  * `-D TDAQ_NO_INSTALL_DEBUG=TRUE` : do not install the debug symbols. The install area will be only ~1GByte.

#### CTest Options

These are the options after `--` on the `build_stack` command line.

  * `-D CTEST_NOCLEAN=TRUE` : do not clear the build directory between calls. Do not use if you can avoid it, e.g. use ccache instead.
  * `-D CTEST_NOTEST=TRUE`  : do not run the tests after the build.
  * `-D CTEST_NO_SUBMIT=TRUE` : do not submit the build and test results to the ATLAS/TDAQ dashboard. Use this if outside of CERN.
  * `-D CTEST_TRACK=Experimental` : change the track on the CDashboard. Experimental is the default.
  * `-D CTEST_CORES=4` : fix the number of cores for parallel build.

## Builds without CVMFS

## Builds without external LCG software

Building without the LCG software stack is non-trivial. It requires to
have all the necessary dependencies available in some other way. This can be
achhieved through OS level installations, or private builds of Boost, ROOT etc.
either manually, or some package manager like conan, vcpkg, spack, ...

Some version indications are given, but in most cases if the OS provides
a recent version it should be good enough.

Build system:

  * gcc >= 13
  * CMake >= 3.27
  * git >= 2.11

General packages (as named in the LCG software):

Note: depending on your Linux distribution you typically need the `foo-dev` or `foo-devel` package.

Note: some software may require some explicit download, e.g. oracle, openjdk is ok for the java dependency

  * Boost  1.82
  * CppUnit
  * cryptography
  * cx_oracle
  * future
  * GSL
  * java = 1.8.x
  * libsodium
  * lxml
  * lz4
  * matplotlib
  * mysql
  * oracle
  * pandas
  * pcre
  * png
  * protobuf
  * pyqt5
  * Python 3.9
  * PyYAML
  * Qt5
  * scipy
  * sqlite
  * tbb
  * XercesC
  * zeromq
  * zlib

HEP specific packages.

  * [ROOT](https://root.cern.ch/) >= 6.28, -D CMAKE_CXX_STANDARD=20
  * [xrootd](https://xrootd.slac.stanford.edu/)

## Manual Build

Assuming all the tools are in the PATH and build dependencies are available, this is what the `build_stack` script
approximately does:

```bash
# checkout
mkdir -p tdaq tdaq-common
git clone https://gitlab.cern.ch/atlas-tdaq-software/tdaq-common-cmake.git tdaq-common/tdaq-common-99-00-00
(cd tdaq-common/tdaq-common-99-00-00; git submodule update -i)
git clone https://gitlab.cern.ch/atlas-tdaq-software/tdaq-cmake.git tdaq/tdaq-99-00-00
(cd tdaq/tdaq-99-00-00; git submodule update -i)

# configure and build tdaq-common
export CMAKE_PREFIX_PATH=`pwd`:tdaq-common/tdaq-common-99-00-00/cmake_tdaq/cmake
mkdir -p build/tdaq-common
cd build/tdaq-common
cmake -D BINARY_TAG=x86_64-el9-gcc13-opt ../../tdaq-common/tdaq-common-99-00-00
make -j $(nproc) -k all install/fast

# configure and build TDAQ
cd ..
mkdir tdaq
cd tdaq
cmake -D BINARY_TAG=x86_64-el9-gcc13-opt ../../tdaq/tdaq-99-00-00
make -j $(nproc) -k all install/fast
```
