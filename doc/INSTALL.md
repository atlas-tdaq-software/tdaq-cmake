# Installation

This is for the *binary* installation of the ATLAS
Trigger/DAQ software. Look at the [build instructions](BUILDING.md)
for how to build and install from source.

## Configurations

The release has typically a build configuration that
is used during data taking at Point 1. At the moment this is
`x86_64-el9-gcc13-opt`. Other compiler versions and `-dbg`
variants are available.

For operating systems: Alma 9 (and compatible clones) on
x86_64 as well as aarch64 are the only officially supported variant.
The aarch64 build is in the early stages of use.

Older releases used in Run2 typically have a slc6 variant instead.
Support for those releases has ended with the start of Run 3.
Support for CentOS 7 releases has ended by December 2023.

## Installation via atlas-dnf5

This is the tool for installation on an Alma 9 or compatible system.

It is a set of wrapper scripts around a (patched) version of
[dnf5](https://github.com/rpm-software-management/dnf5). It allows
installation of the software as non-root user as well as
relocation to an arbitrary location.



### Quick Guide

These instructions assume that you have [apptainer](https://apptainer.org)
available. For other options, see https://gitlab.cern.ch/atlas-tdaq-software/atlas-dnf5 for
more details.

```shell
curl -L -O https://gitlab.cern.ch/api/v4/projects/141622/packages/generic/atlas-dnf5/v5.2.7.0/atlas-dnf5-v5.2.7.0.tar.gz
tar zxf atlas-dnf5-v5.2.7.0.tar.gz
export PATH=$(pwd)/atlas-dnf5-v5.2.7.0:$PATH
```

You have to configure two environment variables: one for the installation directory
location, and one for state directory of `dnf5`. The latter is less critical and
you can delete it if necessary, although things like history and others will be lost.

```shell
export ATLAS_DNF5_INSTALL_DIR=/opt/atlas/tdaq
export ATLAS_DNF5_STATE_DIR=/scratch/dnf5-state
```

The `atlas-dnf5` script is a wrapper around the `dnf5` tool
inside a container. Normally you should use the ATLAS specific
helper scripts instead:

```shell
atlas-install-project tdaq-11-02-01_x86_64-el9-gcc13-opt`
```

This will install the TDAQ software and all dependencies.

`atlas-find-project` can be used to search for projects.

## Installation via (a)yum (OBSOLETE)

The [ayum](https://gitlab.cern.ch/atlas-sit/ayum) tool is a
patched version of yum for SLC6 and CentOS 7. It allows to install
RPMs as a normal user and relocate them to an arbitrary location on disk.

This works for all the LCG and ATLAS RPMs, do not try with any system RPMs !

The tool works completely isolated from the system yum/rpm and does not
require root privileges.

### Configuration

```bash
git clone https://gitlab.cern.ch/atlas-sit/ayum.git
cd ayum
./configure.sh
```

Answer the questions about where you want to install the TDAQ software
and some of the yum/rpm related directories. Defaults are suggested.

Look at the `etc/yum.repos.d/lcg.repo` file and make sure the `prefix`
shares the initial path with the `prefix` in `etc/yum.conf`.

If there are errors the first time you run `ayum`, do this:

```bash
cd src/rpmext
make clean all
```

### Using ayum

Source the setup script in the top-level ayum directory:

```bash
source ./setup.sh
```

Then use the tool as you would use yum:

```bash
ayum list tdaq-10-00-00_x86_64-centos7-gcc11-opt
ayum install tdaq-10-00-00_x86_64-centos7-gcc11-opt
```

There are also two commands, `arpm` and `arepoquery` available that work
like their native counterparts but use the custom RPM and YUM installation
instead.


## Installation of nightly and CI builds.

Both nightly builds and gitlab CI builds are available as downloadable archives
that only need unpacking.

### Nightly builds

This assumes you have CVMFS available for external software.

```bash
mkdir /sw/atlas
cd /sw/atlas
mkdir -p tdaq-common/tdaq-common-99-00-00 tdaq/tdaq-99-00-00
curl -L https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/nightlies/tdaq-common-99-00-00-x86_64-el9-gcc13-opt.tar.gz | (cd tdaq-common/tdaq-common-99-00-00-; tar zxf -)
curl -L https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/nightlies/tdaq-99-00-00-x86_64-el9-gcc13-opt.tar.gz | (cd tdaq/tdaq-99-00-00; tar zxf -)
```
To use it, point the `TDAQ_RELEASE_BASE` variable to the area:

```bash
export TDAQ_RELEASE_BASE=/sw/atlas
cm_setup tdaq-99-00-00
```
In the same way you can the dev4 nightlies by replacing `99-00-00` with `tdaq-99-00-01` everwhere.

### Gitlab CI Builds

After every submission to the tdaq-cmake project a full build is done. The build and test results
are available on the [ATLAS TDAQ CDashboard](https://atlas-tdaq-cdash.web.cern.ch/viewProjects.php).

In case one does not want to wait till the next day until the nightly builds are available from CVMFS,
an archive can be downloaded from gitlab with the build artifacts. One can do this from the web page
showing the pipelines and select the desired build (artifacts are only kept for one day).

The download will be a `artifacts.zip` file that can be unpacked
and used just as described above for the nightly builds.

Note that the gitlab artifacts do not contain debug symbols due to size restrictions.
