# ATLAS Trigger/DAQ software

This is the online software used for data taking
and triggering at the [ATLAS](https://atlas.cern) experiment
at the [LHC](https://lch.cern). The actual high-level
trigger (HLT) algorithms live in the [ATLAS off-line software repository](https://gitlab.cern.ch/atlas/athena).

The source code is at the [CERN Gitlab instance](https://gitlab.cern.ch/atlas-tdaq-software). It
consists of ca. 200 git repositories that are structured via two top-level repositories using
git submodules, [tdaq-common](https://gitlab.cern.ch/atlas-tdaq-software/tdaq-common-cmake) and
[this one](https://gitlab.cern.ch/atlas-tdaq-software/tdaq-cmake).

## Release Notes

Release notes for recent versions are available [here](https://atlas-tdaq-sw-releases.web.cern.ch).

## Supported Environments

The software is strictly driven by the needs of the ATLAS experiment and only the
standard CERN Linux distributions are supported. At the moment this is
Alma 9 (and compatible RHEL clones) for x86_64 and aarch64 architectures.

The software uses a custom stack of external packages that is maintained by the
[CERN SFT group](https://ep-dep-sft.web.cern.ch/project/spi). This stack includes newer versions
of packages than the ones provided by the underlying OS.

Most importantly it expects a newer gcc version (>= 13) and CMake (>= 3.29) to build. All these
tools are available through the [CVMFS file system](https://cernvm.cern.ch/fs/). [Building](doc/BUILDING.md)
the software without this is not suggested.

## Development

ATLAS users should familiarize themselves with the internal [twiki](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltCMake).

## Binary Installation

See the [instructions](doc/INSTALL.md).

## License
The source code is under the Apache 2.0 license, Copyright 2000-2024 CERN for the benefit
of the ATLAS collaboration, except where noted otherwise in the `NOTICE` file of the
rsp. repository.

An effort was made to identify any non-ATLAS code that was integrated over the last
20 years and document copyright and licensing. If anything was missed contact the
e-mail address below.

## Citation

You can cite the software as DOI [10.5281/zenodo.4889103](https://zenodo.org/doi/10.5281/zenodo.4889103).

## Contact

For ATLAS users, use the generic online sw [mailing list](mailto:atlas-tdaq-software@cern.ch).

Please use this generic e-mail address otherwise to contact us: [atdsoft@cern.ch](mailto:atdsoft@cern.ch).
