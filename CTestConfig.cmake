## This file should be placed in the root directory of your project.
## Then modify the CMakeLists.txt file in the root directory of your
## project to incorporate the testing dashboard.
##
## # The following are required to submit to the CDash dashboard:
##   ENABLE_TESTING()
##   INCLUDE(CTest)

set(CTEST_PROJECT_NAME "tdaq")
set(CTEST_NIGHTLY_START_TIME "00:00:00 CEST")

set(CTEST_SUBMIT_URL "https://atlas-tdaq-cdash.web.cern.ch/submit.php?project=tdaq")

set(CTEST_LABELS_FOR_SUBPROJECTS AccessManager ACE ALTI AltiController asyncmsg AutoPrescaleEditor AutoPrescaler BeamSpotUtils beauty CES cmdl cmem_rcc coca coca_mda_admin coldpie config CoolUtils coral_auth dal DAQAssistant DaqDbProxyUtils DAQRelease DAQSWRepository daq_tokens dbe dccommon dcm ddc ddcInfo DFConfiguration DFDebug DFExceptions dfinterface DFSubSystemItem DFThreads DFTools dqm_archive dqm_config dqm_display dqmf DS1WM dvs dvs_gui dvs_tests dynlibs elisa_client_api emon ers2idl EsperUtils EventViewer ExpertSystemInterface felix2atlas felix-interface felix_proxy FELIXPyTools file_sampler GathererProxy genconfig gnam gnamDummyLib HLTMPPU HLTPUDal HLTRC hltsv I2C Igui IguiCommander IguiPanels io_rcc ipc IPCGatewayProxy is ispy jeformat Jers jTestManager L1CTHardwareCompiler log2ers ls mda mda_browser MonInfoGatherer monsvc msgdump mts MuCalStream oh ohp ohpplugins oks oks2coral oksconfig oks_utils OLC2HLT omni omniPy opmon owl PartitionMaker pbeast PmgGui pmgsync ProcessManager pudummy pvss2cool pvss2pbeast QTUtils rcc_corbo rcc_error rcc_rodbusy rcc_time_stamp RCDBitString RCDExampleModules RCDExampleTriggers RCDJtagChain RCDLtp RCDLtpi RCDLtpiModule RCDLTPModule RCDMenu RCDTtc RCDUtilities RCDVme RCInfo RCUtils rdb rdbconfig ResourceManager ResourcesInfo RmGui rn RobinTestSuite RODBusy RODBusyModule ROSApplication ROSasyncmsg ROSBufferManagement ROSCore ROSDescriptor ROSDescriptorNP ROSEventFragment ROSEventInputManager ROSfilar ROSGetInput ROSInterruptScheduler ROSIO ROSMemoryPool ROSMemoryPoolNP ROSModules ROSModulesNP ROSNP2lan ROSObjectAllocation ROSPyTools ROSQuestNP ROSRCDdrivers ROSRobin ROSRobinNP ROSslink ROSsolar ROSTester ROSUtilities RunControl SAReplay SFOng SFOTZ siom sso-helper swrod system TDAQExternal TDAQExtJars TM tmgr transport TrigConfCool TriggerCommander TriggerDB TriggerPanel TRP TTCInfo TTCviModule vme_rcc webemon webis_server wmi xmext)

set(CTEST_USE_LAUNCHERS 1)
