# artificial dependencies

list(APPEND TDAQ_SERIALIZE_TARGETS  rc_FSMRoot rc_FSMLeaf pbeast_read pbeast_read_server pbeast_read_repository pbeast_repository_server pbeast_server pbeast_receiver pbeast_web_receiver external-ipbus)

# check if targets are enabled
set(_targets)
foreach(target ${TDAQ_SERIALIZE_TARGETS})
  if(TARGET ${target})
    get_target_property(imported ${target} IMPORTED)
    if(NOT imported)
      list(APPEND _targets ${target})
    endif()
  endif()
endforeach()


list(LENGTH _targets len)

while(${len} GREATER 1)
  list(GET _targets 0 a)
  list(GET _targets 1 b)
  add_dependencies(${b} ${a})
  list(REMOVE_AT _targets 0)
  list(LENGTH _targets len)
endwhile()

