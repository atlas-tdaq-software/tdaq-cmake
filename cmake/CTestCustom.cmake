
set(CTEST_CUSTOM_ERROR_EXCEPTION ".* Note: No relevant classes found. No output generated.")
set(CTEST_CUSTOM_WARNING_EXCEPTION
   "[0-9]+ warnings?.*"
   "Note: .*"
   "warning: Supported source version 'RELEASE_6' from annotation processor 'org.mangosdk.spi.processor.SpiProcessor' less than.*"
   "warning: Implicitly compiled files were not subject to annotation processing."
   ".*/zmq_utils.h.*"
   ".*/ddc/src/dimlib/.*"
   ".*/tbb/.*/gcc_generic.h:.*warning: extra .*"
   ".*/include/tbb/.*"
   ".*/ROSRobin/.*"
   ".*/ROSfilar/.*"
   ".*/ROSsolar/.*"
   ".*/ROSslink/.*"
   "[WARNING].*"
   "note: ' *[0-9]+ .*"
)
set(CTEST_CUSTOM_MAXIMUM_NUMBER_OF_WARNINGS 5000)
set(CTEST_CUSTOM_MAXIMUM_NUMBER_OF_ERRORS 500)
