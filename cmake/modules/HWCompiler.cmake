#
# This is the generation function for the HW compiler from the L1CT group.
#
function(l1ct_generate_source_files sname dname xfile)

    set(GSRCF_NAMESPACE)
    cmake_parse_arguments(GSRCF "" "" "NAMESPACE;OPTIONS;ADD_DEPENDENCIES;" ${ARGN})

    message(STATUS "    Adding generation of ${sname} source files from ${xfile}")

    set(nspace LVL1)
    if(GSRCF_NAMESPACE)
        set(nspace ${GSRCF_NAMESPACE})
    endif()

    # Make sure the compiler is build first before we run it
    if(TDAQ_HAVE_L1CTHardwareCompiler)
      set(_l1cthw_deps runL1CTHardwareCompiler)
    endif()

    file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${dname} ${CMAKE_CURRENT_BINARY_DIR}/src ${CMAKE_CURRENT_BINARY_DIR}/src/test)

    add_custom_command (
        OUTPUT ${dname}/${sname}.h
               ${dname}/${sname}_BITSTRING.h
               src/${sname}.cc
               src/${sname}_BITSTRING.cc
               src/test/menu${sname}.cc
        COMMAND runL1CTHardwareCompiler -f ${CMAKE_CURRENT_SOURCE_DIR}/data/${xfile} -F ${sname} -P ${dname} -N ${nspace} ${GSRCF_OPTIONS}
        COMMAND ${CMAKE_COMMAND} -E rename ${sname}.h ${dname}/${sname}.h
        COMMAND ${CMAKE_COMMAND} -E rename ${sname}_BITSTRING.h ${dname}/${sname}_BITSTRING.h
        COMMAND ${CMAKE_COMMAND} -E rename ${sname}.cc src/${sname}.cc
        COMMAND ${CMAKE_COMMAND} -E rename ${sname}_BITSTRING.cc src/${sname}_BITSTRING.cc
        COMMAND ${CMAKE_COMMAND} -E rename menu${sname}.cc src/test/menu${sname}.cc
        COMMENT "generating ${sname} source files from ${xfile}"
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/data/${xfile} ${GSRCF_ADD_DEPENDENCIES} ${_l1cthw_deps}
    )

endfunction(l1ct_generate_source_files)


