# - Make the pyqt5 environment and tools available.
#
# It also provides the functions gen_pyqt_resource and gen_pyqt_uic.
#
# The following variables are defined:
#
#   PYQT5_FOUND
#   PYQT5_PYTHON_PATH
#   PYQT5_BINARY_PATH
#
# Commands:
#
#   pyrcc_cmd
#   pyuic_cmd

if(PYQT5_FOUND)
  return()
endif()

set(PYQT5_FOUND 1)

file(GLOB PYQT5_PYTHON_PATH "${PYQT5_ROOT}/lib/python*/site-packages")

mark_as_advanced(PYQT5_FOUND PYQT5_PYTHON_PATH)

# Provides functions to compile .qrc and .ui files into Python modules.

find_package(PythonInterp QUIET REQUIRED)

find_program(pyrcc_cmd pyrcc5 HINTS ${PYQT5_ROOT}/bin)
mark_as_advanced(pyrcc_cmd)
find_program(pyuic_cmd pyuic5 HINTS ${PYQT5_ROOT}/bin)
mark_as_advanced(pyuic_cmd)

get_filename_component(PYQT5_BINARY_PATH ${pyrcc_cmd} PATH)

