
# TDAQ variant for HLT.

This variant tries to build a minimal set of package that is needed by HLT.

Replace the version numbers with 99-00-00 to build the nightly version.

The options are `--` disable tests and submission the the dashboard (e.g. if you are outside of CERN).

## Building on Intel

```bash
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/build_stack --variant=hlt tdaq-common-04-04-00 tdaq-09-04-00 x86_64-centos7-gcc11-opt -D LCG_VERSION_CONFIG=LCG_101 -- -D CTEST_NOTESTS=1 -D CTEST_NO_SUBMIT=1 
```

## Buildin on ARM (aarch64) natively

```bash
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/build_stack --variant=hlt tdaq-common-04-04-00 tdaq-09-04-00 aarch64-centos7-gcc11-opt -D LCG_VERSION_CONFIG=LCG_101arm -D TDAQ_JAVA_HOME=/usr/lib/jvm/java-1.8.0 -D MAVEN_ROOT=/cvmfs/sft.cern.ch/lcg/releases/LCG_101arm/maven/3.6.1/x86_64-centos7-gcc11-opt -- -D CTEST_NOTESTS=1 -D CTEST_NO_SUBMIT=1
```

Cross-compiling this variant is not supported.
