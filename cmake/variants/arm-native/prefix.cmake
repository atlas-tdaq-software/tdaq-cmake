#
# This is an optional include file for aarch64 builds.
# It disables various targets to break further dependecies and avoid having to pull in more packages.
#
list(APPEND TDAQ_DISABLED_TARGETS RODBusyModule test_rodbusy_action DefaultBusyTimeoutAction rc_FSMRoot rc_RootCtrl rc_root_controller rc_test_fsm mts2splunk_receiver)
