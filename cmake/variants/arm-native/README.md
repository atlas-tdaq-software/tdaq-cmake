# ARM native build

An evolving list of packages that compile on ARM natively with work-arounds
for missing tools from TDAQ and/or LCG.

The packages are a superset of the `arm` and the `hlt` variants.

## Tools

 * CMake 
   * export CMAKE_PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/1.17.1/Linux-aarch/bin before setup/build
 * Maven 
   * Add -D MAVEN_ROOT=/cvmfs/sft.cern.ch/lcg/LCG_98/maven/*/x86_64-centos7-gcc8-opt/
   * or install maven locally: `sudo yum install -y maven`
 * Java
   * Install locally, and add -D TDAQ_JAVA_HOME=/usr/lib/jvm/java-1.8.0 

## LCG Version

For now one has to use one of the nightly LCG devARM builds.

```bash
export CMAKE_PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/1.17.1/Linux-aarch/bin
/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/build_stack \
  --variant=arm-native tdaq-common-99-00-00 tdaq-99-00-00 aarch64-centos7-gcc8-opt \
  -D LCG_VERSION_CONFIG=LCG_999devARM/Fri \
  -D MAVEN_ROOT=/cvmfs/sft.cern.ch/lcg/LCG_98/maven/*/x86_64-centos7-gcc8-opt \
  -D TDAQ_JAVA_HOME=/usr/lib/jvm/java-1.8.0 \
  -- -D CTEST_CORES=4
```

