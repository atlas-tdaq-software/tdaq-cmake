#
# This is an optional include file for aarch64 builds.
#
list(APPEND TDAQ_DISABLED_TARGETS RODBusyModule test_rodbusy_action DefaultBusyTimeoutAction rc_RootCtrl rc_root_controller roks OKS_commit OKS_commit_exec oks_report_equal_objects oks_put_schema oks_put_data oks_get_schema oks_get_data oks_tag_data oks_ls_data oks_report_bad_classes oks_merge oks_diff_data oks_diff_schema oks_tutorial oks_utils_alloc_test xmoks is_monitor oh_display rc_FSMRoot rc_test_fsm oks_data_editor oks_schema_editor mts2splunk_receiver)

# until Giuseppe fixes it in RunControl package
add_custom_target(rc_FSMRoot)
