# TDAQ variant for ARM SoC

This provides enough functionality to build a run controlled
application.

This variant can be cross-compiled, it is the basis of the 
nightly ARM build on /cvmfs/atlas-online-nightlies.cern.ch/tdaq/arm

