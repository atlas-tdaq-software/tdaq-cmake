
find_path(DETCOMMON_PATH
  NAMES DetCommonConfig-targets.cmake
  HINTS 
     ${ATLAS_RELEASE_BASE}/${PACKAGE_FIND_VERSION_MAJOR}.${PACKAGE_FIND_VERSION_MINOR}/DetCommon/${PACKAGE_FIND_VERSION}/InstallArea/${BINARY_TAG}/cmake
     $ENV{ATLAS_RELEASE_BASE}/${PACKAGE_FIND_VERSION_MAJOR}.${PACKAGE_FIND_VERSION_MINOR}/DetCommon/${PACKAGE_FIND_VERSION}/InstallArea/${BINARY_TAG}/cmake
     /cvmfs/atlas.cern.ch/repo/sw/software/${PACKAGE_FIND_VERSION_MAJOR}.${PACKAGE_FIND_VERSION_MINOR}/DetCommon/${PACKAGE_FIND_VERSION}/InstallArea/${BINARY_TAG}/cmake)

if(DETCOMMON_PATH)

  get_filename_component(DETCOMMON_INST_PATH ${DETCOMMON_PATH} DIRECTORY CACHE)

  set(PACKAGE_VERSION ${PACKAGE_FIND_VERSION})
  set(PACKAGE_VERSION_COMPATIBLE TRUE )
  set(PACKAGE_VERSION_EXACT TRUE )

  set(DetCommon_VERSION ${PACKAGE_FIND_VERSION})
  set(DetCommon_VERSION_MAJOR ${PACKAGE_FIND_VERSION_MAJOR})
  set(DetCommon_VERSION_MINOR ${PACKAGE_FIND_VERSION_MINOR})
  set(DetCommon_VERSION_PATCH ${PACKAGE_FIND_VERSION_PATCH})
  set(DetCommon_VERSION_TWEAK 0)
  set(DetCommon_VERSION_COUNT 3)
endif()
